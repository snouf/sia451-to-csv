#!/usr/bin/env python
# -*- coding: utf-8 -*-

#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

from xml.dom import minidom
from datetime import datetime
import zipfile, sys, os.path, wx, re
from getopt import getopt
import tkFileDialog as Selector
import logging
logging.basicConfig(level=logging.DEBUG,format='%(levelname)s line %(lineno)d - %(message)s',filename='debug.log')
import pdb
from ConfigParser import RawConfigParser
import UNECE20Code

#pdb.set_trace()

__APPNAME__ = u'SIA451-to-CSV'
__HOMEPAGE__ = u'http://jonas.tuxfamily.org/wiki/sia451_to_csv'
__VERSION__ = u'0.20110723'
__AUTHOR__ = u'Jonas Fourquier <jonas@tuxfamily.org>'
__PURPOSE__ = u'''\
Ce programme permet de convertir des fichiers SIA451 (xml ou txt) en
fichiers CSV.'''
__LICENCE__ = u'''\
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
MA 02110-1301, USA.'''


class gui(wx.Frame):
    #http://www.wxpython.org/docs/api/
    def __init__(self,parent,id,title):
        wx.Frame.__init__(self,parent,id,title)
        self.parent = parent
        self.initialize()


    def initialize(self):
        fileMenu = wx.Menu()
        self.Bind(wx.EVT_MENU, self.OnExit,     fileMenu.Append(wx.ID_EXIT))

        helpMenu = wx.Menu()
        self.Bind(wx.EVT_MENU, self.OnHelp,     helpMenu.Append(wx.ID_HELP))
        self.Bind(wx.EVT_MENU, self.OnWebsite,  helpMenu.Append(wx.ID_HOME,"Site &Web"," Ouvrir le site web"))
        self.Bind(wx.EVT_MENU, self.OnAbout,    helpMenu.Append(wx.ID_ABOUT))

        menuBar = wx.MenuBar()
        menuBar.Append(fileMenu,u"&Fichier")
        menuBar.Append(helpMenu,u"A&ide")
        self.SetMenuBar(menuBar)
        menuBar.Show()

        self.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BTNFACE))

        sizer = wx.GridBagSizer()
        #             0                 1
        # 0 | Fichier SIA451xml :               |
        # 1 | ...............   | Parcourir...  |
        # 2 |-----------------------------------|
        # 3 | Dossier d\'exportation :          |
        # 4 | ...............   | Parcourir...  |
        # 5 |-----------------------------------|
        # 6 |              Exporter             |

        # 0
        labelSIA451 = wx.StaticText(self,-1,label=u'Fichier SIA451 :')
        sizer.Add(labelSIA451,pos=(0,0),flag=wx.EXPAND|wx.ALL,border=3)

        # 1
        self.entrySIA451 = wx.TextCtrl(self,-1,value="")
        sizer.Add(self.entrySIA451,pos=(1,0),flag=wx.EXPAND|wx.ALL,border=3)

        button = wx.Button(self,-1,label=u"Parcourir ...")
        sizer.Add(button, pos=(1,1),flag=wx.EXPAND|wx.ALL,border=3)
        self.Bind(wx.EVT_BUTTON, self.OnButtonSelectSIA451, button)

        # 2
        line = wx.StaticLine(self)
        sizer.Add(line,pos=(2,0),span=(1,2),flag=wx.EXPAND)

        # 3
        labelDirExport = wx.StaticText(self,-1,label=u'Dossier d\'exportation :')
        sizer.Add(labelDirExport,pos=(3,0),flag=wx.EXPAND|wx.ALL,border=3)

        # 4
        self.entryDirExport = wx.TextCtrl(self,-1,value="")
        sizer.Add(self.entryDirExport,pos=(4,0),flag=wx.EXPAND|wx.ALL,border=3)

        button = wx.Button(self,-1,label=u"Parcourir ...")
        sizer.Add(button,pos=(4,1),flag=wx.EXPAND|wx.ALL,border=3)
        self.Bind(wx.EVT_BUTTON, self.OnButtonDirExport, button)

        # 5
        line = wx.StaticLine(self)
        sizer.Add(line,pos=(5,0),span=(1,2),flag=wx.EXPAND)

        # 6
        button = wx.Button(self,-1,label=u"Exporter")
        sizer.Add(button,pos=(6,0),span=(1,2),flag=wx.EXPAND|wx.ALL,border=3)
        self.Bind(wx.EVT_BUTTON, self.OnButtonExport, button)

        sizer.AddGrowableCol(0)
        self.SetSizerAndFit(sizer)
        self.SetSizeHints(-1,self.GetSize().y,-1,self.GetSize().y );

        self.Show(True)

    #~ def OnSize(self,event,sizer):
        #~ self.SetSizerAndFit(sizer)


    def OnHelp(self,event):
        dialog = wx.MessageDialog(
                parent = self,
                message = u'\
Séléctionnez votre fichier SIA451, le dossier d\'exportation puis cliquez sur \
"Exporter".\n\
Vous trouverez alors dans le dossier d\'exportation un fichier CSV par CAN. \
Ces fichiers peuvent être ouvert avec le plupat des tableurs (LibreOffice, \
Openoffice.org, Microsoft office ...)\n\
\n\
Les sorties csv peuvent être personnalisées (voir sia451-to-csv.cfg) et ce programme \
peux être utilisé en ligne de commande (voir "%s -h")'%__file__,
                style=wx.OK|wx.ICON_INFORMATION
            )
        dialog.ShowModal()
        dialog.Destroy()

    def OnWebsite(self,event):
        import webbrowser
        webbrowser.open(__HOMEPAGE__)

    def OnAbout(self,event):
        aboutInfo = wx.AboutDialogInfo()
        aboutInfo.AddDeveloper(__AUTHOR__)
        aboutInfo.SetName(__APPNAME__)
        aboutInfo.SetVersion(__VERSION__)
        aboutInfo.SetDescription(__PURPOSE__)
        aboutInfo.SetLicence(__LICENCE__)
        wx.AboutBox(aboutInfo)

    def OnExit(self,event):
        self.Close()
        exit(0)

    def OnButtonSelectSIA451(self,event):
        fileSIA451 = wx.LoadFileSelector(
                what = u"Séléctionner votre fichier SIA451xml",
                default_name = self.entrySIA451.GetValue(),
                extension = u"SIA451 (*.451;*.01S)|*.451;*.01S|All|*"
            )
        if fileSIA451 :
            self.entrySIA451.SetValue(fileSIA451)
            if not self.entryDirExport.GetValue() :
                self.entryDirExport.SetValue(os.path.split(fileSIA451)[0])

    def OnButtonDirExport(self,event):
        entryDirExport = wx.DirSelector(
                message = u"Séléctionner les dossiers d'exportation",
                defaultPath = self.entryDirExport.GetValue()
            )
        if entryDirExport:
            self.entryDirExport.SetValue(entryDirExport)

    def OnButtonExport(self,event):
        sia451 = load_sia451autoType(self.entrySIA451.GetValue())
        logging.debug('sia451')
        logging.debug(sia451)
        if len(sia451.errors) == 0 :
            write_csv(self.entryDirExport.GetValue(), sia451.meta, sia451.chapters)
            dialog = wx.MessageDialog(parent=self,
                    message = u'Fichier exporté avec succès.\nSouhaitez vous ouvrir le dossier où se trouve les fichiers exportés ?',
                    style=wx.YES_NO|wx.ICON_INFORMATION
                )
            if dialog.ShowModal() == wx.ID_YES :
                if os.name == "nt":
                    os.system('explorer "%s"'%self.entryDirExport.GetValue())
                elif os.name == "posix":
                    os.system('xdg-open "%s"'%self.entryDirExport.GetValue())
                elif os.name == 'os2' :
                    os.system('open "%s"'%self.entryDirExport.GetValue())
            dialog.Destroy()
        else :
            dialog = wx.MessageDialog(parent=self,
                    message = u'L\'exportation a échoué :\n%s'%"\n".join(sia451.errors),
                    style=wx.OK|wx.ICON_ERROR
                )
            dialog.ShowModal()
            dialog.Destroy()


class load_sia451txt :
    global charset

    def __init__(self,ftxt) :

        self.errors = []
        self.meta = {}
        self.part = {}
        self.chapters = {}

        file = open(ftxt,'r')
        for line in file :
            line = line.decode('IBM850').encode(charset,'xmlcharrefreplace')
            logging.debug('Parse : %s'%line)
            if line[0] == 'A' :
                # Entête avec nom du projet
                self.meta = {
                        'projetName': line[92:122].strip(),
                        'projetLongName': line[92:122].strip(),
                        'awardCode': line[122:137].strip().lstrip('0'),
                        'awardName': line[137:167].strip(),
                        'awardDescription':'-',
                        'awardDate':'-'
                    }

            if line[0] == 'B' :
                # Partie de l'ouvrage
                notation = line[17:41].strip()
                self.part[notation] = {
                        'notation':notation,
                        'title':line[92:].strip()
                    }

            if line[0] == 'G' :
                # Article

                SIAtype = int(line[41:42])

                if SIAtype == 1 :
                    # tête de chapitre
                    chapterNr = line[1:4].strip()
                    self.chapters[chapterNr] = {
                        'nr': chapterNr,
                        'name': line[92:].strip(),
                        'year': line[5:7].strip(),
                        'content':{}
                    }
                else :
                    posNr = line[7:13].strip()
                    if not self.chapters[chapterNr]['content'].has_key(posNr) :
                        #Création de la position
                        self.chapters[chapterNr]['content'][posNr] = {
                                'nr': posNr,
                                'txt': '',
                                'quantity' : [],
                                'unit': '',
                            }

                    if SIAtype == 2 and config.get('position','description') == 'singleline' :
                        # description simple ligne
                        self.chapters[chapterNr]['content'][posNr]['txt'] = line[92:].strip()

                    elif SIAtype == 3 and config.get('position','description') == 'multiline' :
                        # description multiligne
                        self.chapters[chapterNr]['content'][posNr]['txt'] += "\n"+line[92:].strip()
                        self.chapters[chapterNr]['content'][posNr]['txt'] = self.chapters[chapterNr]['content'][posNr]['txt'].strip()

                    elif SIAtype == 4 :
                        # inconnu
                        self.errors.append('Entrée de type 4 inconnue. Merci de contacter le développeur')

                    elif SIAtype == 5 :
                        # unitée
                        posUnit =line[43:].strip()

                    elif SIAtype == 6 :
                        # la quantité
                        if line[45:58].strip() != '' :
                            if line[44] == '+' :
                                quantity = line[45:55].lstrip('0')+'.'+line[55:58]
                            elif line[44] == '-' :
                                quantity = '-'+line[45:55].lstrip('0')+'.'+line[55:58]
                            else :
                                quantity = line[44:56].lstrip('0')+'.'+line[56:58]
                        else :
                            quantity = ''

                        # Le prix unitaire
                        if line[62:71].strip() != '' :
                            if line[61] == '+' :
                                unitprice = line[62:72].lstrip('0')+'.'+line[72:].rstrip()
                            elif line[61] == '-' :
                                unitprice = '-'+line[62:72].lstrip('0')+'.'+line[72:].rstrip()
                            else :
                                unitprice = line[61:71].lstrip('0')+'.'+line[72:].rstrip()
                        else :
                            unitprice = ''

                        self.chapters[chapterNr]['content'][posNr]['quantity'].append({
                                'partabrev' : line[17:41].strip(),
                                'parttitle' : '',
                                'quantity' : quantity,
                                'unit': posUnit,
                                'quantitytype' : line[43],
                                'pricetype' : line[60],
                                'unitprice' : unitprice
                            })


        #trie les articles
        for chapterID in self.chapters.keys() :
            self.chapters[chapterID]['content'] = self.chapters[chapterID]['content'].items()
            self.chapters[chapterID]['content'].sort()


class load_sia451xml :
    global charset

    def __init__(self,fzip) :

        def childs_by_tagname(node,tagname) :
            childs = []
            for child in node.childNodes :
                if child.nodeType == child.ELEMENT_NODE and child.tagName == tagname :
                    childs.append(child)
            return childs

        def getStrText(node) :
            if node.hasChildNodes() :
                child = node.firstChild
                if child.nodeType == child.TEXT_NODE :
                    return child.data.encode(charset,'xmlcharrefreplace')
            logging.warning('Can\'t read string, load empty value\n\t- node: %s'%node.toxml())
            return ''


        def getStrNode(parent,nodeName) :
            nodes = parent.getElementsByTagName(nodeName)
            if len(nodes) :
                child = nodes[0].firstChild
                if child.nodeType == child.TEXT_NODE :
                    return child.data.encode(charset,'xmlcharrefreplace')
                else :
                    return ''
            else :
                return ''


        def getIntNode(parent,nodeName) :
            nodes = parent.getElementsByTagName(nodeName)
            if len(nodes) :
                child = nodes[0].firstChild
                if child.nodeType == child.TEXT_NODE :
                    try :
                        return int(child.data)
                    except :
                        return 0
                else :
                    return 0
            else :
                return 0


        def getStrAttribute(node, attribute) :
            try :
                return str(node.getAttribute(attribute)).encode(charset,'xmlcharrefreplace')
            except Exception, e:
                logging.warning('Can\'t load string in attribute "%s" replace by empty string\n\t- error: %s\n\t- node: %s)'%(attribute,e,node.toxml()))
                return ''


        def set_key_by_id(id,dico) :
            for (chapterid,chapter) in self.chapters.items() :
                for (posNr,pos) in chapter['content'].items() :
                    if id in pos['ids'] :
                        logging.debug('Add to %s'%posNr)
                        logging.debug(dico)
                        self.chapters[chapterid]['content'][posNr].update(dico)
                        return True
            logging.warning('Can\'t find pos with id %s'%id)
            return False

        def get_classItem(id) :
            for classitem in self.part.values() :
                if id in classitem['relQuantity'] :
                    return classitem

        def append_quantity(id, ref, quantity, quantityunit, quantitytype, pricetype, price) :
            #trouver la position
            posNr = None
            for (tmpChapterID,chapter) in self.chapters.items() :
                for (tmpPosNr,pos) in chapter['content'].items() :
                    if ref in pos['ids'] :
                        posNr = tmpPosNr
                        chapterID = tmpChapterID
                        break
                if posNr != None :
                    break

            logging.debug('is quantity to pos %s'%posNr)

            #trouver la partie

            for tmpclassitem in self.part.values() :
                if id in tmpclassitem['relQuantity'] :
                    #~ logging.debug(ref)
                    #~ logging.debug('in')
                    #~ logging.debug(tmpclassitem)
                    classitem = tmpclassitem
                    break
            logging.debug('is quantity to part %s'%classitem['notation'])

            self.chapters[chapterID]['content'][posNr]['quantity'].append({
                    'partabrev' : classitem['notation'],
                    'parttitle' : classitem['title'],
                    'quantity' : quantity,
                    'unit': UNECE20Code.get_unit_by_code(quantityunit),
                    'quantitytype' : quantitytype,
                    'pricetype' : pricetype,
                    'unitprice' : price
                })


        self.errors = []
        self.meta = {}
        self.chapters = {}
        self.part = {}


        #~ try:
        fz = zipfile.ZipFile(fzip,'r')
        #~ pdb.set_trace()
        xmldoc = minidom.parse(fz.open('x451.xml'))
        fz.close()

        xmlProject = xmldoc.getElementsByTagName('X451Project')[0]
        xmlAward = xmldoc.getElementsByTagName('X451Award')[0]
        self.meta = {
                'projetName': getStrNode(xmlProject,'Name'),
                'projetLongName': getStrNode(xmlProject,'LongName'),
                'awardCode': getStrNode(xmlAward,'AwardCode'),
                'awardName': getStrNode(xmlAward,'AwardName'),
                'awardDescription': getStrNode(xmlAward,'Description'),
                'awardDate':datetime.fromtimestamp(int(getStrNode(xmlAward,'DocCreateDate'))).strftime(config.get('export_csv','date_format'))
            }

        xml451 = childs_by_tagname(xmldoc,'X451')[0]


        # Parcours les noeuds <X451CatChapter> pour trouver les chapitres
        for xmlChapter in childs_by_tagname(xml451,'X451CatChapter') :
            chapterID = getStrAttribute(xmlChapter,'id')
            nr = getStrNode(xmlChapter,'ChapterNr')
            name = getStrNode(xmlChapter,'ChapterName')
            year = getStrNode(xmlChapter,'Revision')
            self.chapters[chapterID] = {
                    'nr':nr,
                    'name':name,
                    'year':year,
                    'content':{}
                }
            logging.debug("Find chapter : %(nr)s - %(name)s"%self.chapters[chapterID])


        # Parcours les noeuds <X451ClassificationItem> pour trouver les partie
        for X451ClassificationItem in childs_by_tagname(xml451,'X451ClassificationItem') :
            classItemID = getStrAttribute(X451ClassificationItem,'id')
            self.part[classItemID] = {
                    'notation':getStrNode(X451ClassificationItem,'Notation'),
                    'title':getStrNode(X451ClassificationItem,'Title'),
                    'relQuantity':[]
                }


        # Parcours les noeuds <X451RelAssociatesClassification> pour trouver les ref de position faisant partie de la partie
        for X451RelAssociatesClassification in childs_by_tagname(xml451,'X451RelAssociatesClassification') :
            RelatedClassificationItem = childs_by_tagname(X451RelAssociatesClassification,'RelatedClassificationItem')[0]
            X451ClassificationItem = childs_by_tagname(RelatedClassificationItem,'X451ClassificationItem')[0]
            classItemRef = getStrAttribute(X451ClassificationItem,'ref')
            RelatedObjects = childs_by_tagname(X451RelAssociatesClassification,'RelatedObjects')[0]
            for X451Quantity in childs_by_tagname(RelatedObjects,'X451Quantity') :
                self.part[classItemRef]['relQuantity'].append(getStrAttribute(X451Quantity,'ref'))


        # Parcours les noeuds <X451CatPos> pour trouver les position
        for xmlPos in childs_by_tagname(xml451,'X451CatPos') :
            posNr = getStrNode(xmlPos,'LPosNr')
            posRef = getStrAttribute(xmlPos,'ref')

            if self.chapters[posRef]['content'].has_key(posNr) :
                self.chapters[posRef]['content'][posNr].update({
                        'nr': posNr,
                        'unit': UNECE20Code.get_unit_by_code(getStrNode(xmlPos,'QuantityUnit')),
                        'unitprice': getStrNode(xmlPos,'QuantityPrice')
                    })
                self.chapters[posRef]['content'][posNr]['ids'].append(getStrAttribute(xmlPos,'id'))
                self.chapters[posRef]['content'][posNr]['txt'].append((getStrNode(xmlPos,'VarNr'),getStrNode(xmlPos,'PosTxt')))
            else :
                self.chapters[posRef]['content'][posNr] = {
                        'ids' : [getStrAttribute(xmlPos,'id')],
                        'nr': posNr,
                        'txt': [(getStrNode(xmlPos,'VarNr'),getStrNode(xmlPos,'PosTxt'))],
                        'quantity' : [],
                        'unit': UNECE20Code.get_unit_by_code(getStrNode(xmlPos,'QuantityUnit')),
                    }


        # Parcours les noeuds <X451Quantity> pour trouver les position
        for xmlQuantity in childs_by_tagname(xml451,'X451Quantity') :
            append_quantity(
                    id = getStrAttribute(xmlQuantity,'id'),
                    ref = getStrAttribute(xmlQuantity,'ref'),
                    quantityunit = getStrNode(xmlQuantity,'QuantityUnit'),
                    quantity = getStrNode(xmlQuantity,'Quantity'),
                    quantitytype = getStrNode(xmlQuantity,'QuantityType'),
                    pricetype = getStrNode(xmlQuantity,'PriceType'),
                    price = getStrNode(xmlQuantity,'Price') #FIXME j'ai pas de prix dans le fichier exeample
                )


        #trie les chapire et compacte les descritions
        for chapterID in self.chapters.keys() :
            for (posNr,pos) in self.chapters[chapterID]['content'].items() :
                self.chapters[chapterID]['content'][posNr]['txt'].sort()
                txt = ''
                for (keysort,text) in self.chapters[chapterID]['content'][posNr]['txt'] :
                    if txt :
                        txt += "\n"+text
                    else :
                        txt = text
                self.chapters[chapterID]['content'][posNr]['txt'] = txt
            self.chapters[chapterID]['content'] = self.chapters[chapterID]['content'].items()
            self.chapters[chapterID]['content'].sort()


        #~ except Exception, e:
            #~ self.errors.append(e.message)
            #~ logging.error(e.message)


class load_sia451autoType (load_sia451txt, load_sia451xml):
    def __init__(self, file,type='') :
        logging.debug('Detect type of %s'%file)
        ext = os.path.splitext(file)[1]
        logging.debug('... Type is %s'%ext)
        if ext == '.01S' :
            load_sia451txt.__init__(self,file)
        elif ext == '.451' :
            load_sia451xml.__init__(self,file)
        else :
            self.errors = [u'Type de fichier inconnu, séléctionnez un fichier *.01S ou *.451.']


def write_csv (outputdir,meta,chapters):
    #~ def addslashes(s):
        #~ d = {'"':'\\"', "'":"\\'", "\0":"\\\0", "\\":"\\\\"}
        #~ return ''.join(d.get(c, c) for c in s)

    quantity_not_count_type = config.get('quantity','not_count_type').split(',')
    price_not_count_type = config.get('unitprice','not_count_type').split(',')

    for chapter in chapters.values() :
        output_content = ""
        chapter['total'] = 0.
        filename = re.sub('[^a-zA-Z0-9_\-.]+','_',"%s - %s.csv"%(chapter['nr'],chapter['name']))
        filename = filename.decode(charset)
        logging.debug('Writing: "%s"'%filename)

        #~ list_pos=chapter['content'].items()
        #~ list_pos.sort()
        for (posnr,pos) in chapter['content'] :
            #Format le numéro d'article
            try:
                pos['nr'] = config.get('position','dot').join([pos['nr'][0:3],pos['nr'][3:]])
            except:
                pass

            #Format les champs
            pos['txt'] = pos['txt'].replace('"','\'\'')
            postxt4csv = {}
            output_content += config.get('export_csv','table_content_txt').replace('\\n',"\n").replace('\\r',"\r")%pos+"\n"

            for quanity in  pos['quantity'] :
                #Calcul le produit
                if (quanity['quantity']) and (quanity['unitprice']) :
                    produit = float(quanity['quantity']) * float(quanity['unitprice'])
                    quanity['price'] = ('%.2f'%produit).replace('.',config.get('export_csv','dot'))
                    chapter['total'] += produit
                elif pos['nr'][-1] == '0' :
                    quanity['price'] = ''
                else :
                    quanity['price'] = config.get('price','empty_replace_by')

                # type de quantité
                if (quanity['quantitytype'] in quantity_not_count_type) or (quanity['pricetype'] in price_not_count_type) :
                    quanity['price'] = config.get('price','not_count_format')%quanity['price']
                    quanity['in_total'] = '0'
                    quanity['is_variante'] = '1'
                else :
                    quanity['in_total'] = '1'
                    quanity['is_variante'] = '0'

                #Format la quantité
                if quanity['quantity'] != '' :
                    quanity['quantity'] = quanity['quantity'].replace('.',config.get('export_csv','dot'))
                else :
                    quanity['quantity'] = config.get('quantity','empty_replace_by')
                if quanity['quantitytype'] in quantity_not_count_type :
                    quanity['quantity'] = config.get('quantity','not_count_format')%quanity['quantity']

                #Format la prix unitaire
                if quanity['unitprice'] != '' :
                    quanity['unitprice'] = quanity['unitprice'].replace('.',config.get('export_csv','dot'))
                else :
                    quanity['unitprice'] = config.get('unitprice','empty_replace_by')
                if quanity['unitprice'] in price_not_count_type :
                    quanity['unitprice'] = config.get('unitprice','not_count_format')%quanity['unitprice']

                output_content += config.get('export_csv','table_content_quantity').replace('\\n',"\n").replace('\\r',"\r")%quanity+"\n"



        if chapter['total'] == 0. :
            chapter['total'] = (config.get('unitprice','empty_replace_by')).replace('.',config.get('export_csv','dot'))

        output = config.get('export_csv','project_head').replace('\\n',"\n").replace('\\r',"\r")%meta+"\n"
        output += config.get('export_csv','chapter_head').replace('\\n',"\n").replace('\\r',"\r")%chapter+"\n"
        output += config.get('export_csv','table_head').replace('\\n',"\n").replace('\\r',"\r")%chapter+"\n"
        output += output_content
        output += config.get('export_csv','table_footer').replace('\\n',"\n").replace('\\r',"\r")%chapter+"\n"

        fp = open(os.path.join(outputdir,filename),'w')
        fp.write(output)
        fp.close()




def usage() :
    print u'''\
%(title)s (v%(version)s) by %(author)s
homepage: %(homepage)s

%(purpose)s

Usage with GUI:
    %(file)s [OPTIONS] [FILE]
Usage in commande line:
    %(file)s --nogui [OPTIONS] FILES

GUI support only one FILE, Commande line support one or more FILES

OPTIONS :
    -o, --output      Directory for exportation (default: directory of file)
    -c, --charset     Encoding of output files (default: UTF-8 or ISO 8859-1 on windows)
    -p, --preference  a specific configuration file (see sia451-to-csv.cfg for an example)
    -l, --licence     Display licence
    -d, --debug       Display some infos\
'''%{
        'title':__APPNAME__,
        'version':__VERSION__,
        'author':__AUTHOR__,
        'homepage':__HOMEPAGE__,
        'purpose':__PURPOSE__,
        'file':__file__
    }


def licence() :
    print __LICENCE__


if __name__ == "__main__":
    nogui = False
    outputdir = None
    inputfiles = []
    charset = None
    preferenceFile = 'sia451-to-csv.cfg'

    opts,args = getopt(sys.argv[1:], "hldo:c:p:",["help","licence","debug","nogui","output=","charset=","preference="])

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit(0)
        elif opt in ("-l", "--licence"):
            licence()
            sys.exit(0)
        elif opt in ("-d", "--debug"):
            logging.basicConfig(level=logging.DEBUG)
            logging.debug("debug mode actived")
        elif opt in ("--nogui"):
            nogui = True
        elif opt in ("-o", "--output"):
            outputdir = arg
        elif opt in ("-c", "--charset"):
            charset = arg
        elif opt in ("-p", "--preference"):
            preferenceFile = arg
    inputfiles = args

    if not charset :
        if sys.platform == 'win32' :
            charset = 'ISO 8859-1'
        else :
            charset = 'UTF-8'

    config = RawConfigParser()
    config.read(preferenceFile)

    if not nogui :
        logging.debug('Start GUI')
        app = wx.App()
        wxframe = gui(None,-1,__APPNAME__)
        if len(inputfiles) == 1:
            wxframe.entrySIA451.SetValue(inputfiles[0])
        elif len(inputfiles) > 1:
            logging.warning('Only one file is supported by GUI')
            wxframe.entrySIA451.SetValue(inputfiles[0])
        if outputdir :
            wxframe.entryDirExport.SetValue(outputdir)
        elif len(inputfiles) >= 1:
            wxframe.entryDirExport.SetValue(os.path.split(inputfiles[0])[0])
        app.MainLoop()
    else :
        if len(inputfiles) == 0:
            usage()

        for inputfile in inputfiles :
            if not outputdir:
                outputdir = os.path.split(inputfile)[0]
            logging.debug('Export "%s" to "%s"'%(inputfile,outputdir))
            sia451 = load_sia451autoType(inputfile)
            logging.debug(sia451)
            if len(sia451.errors) == 0 :
                write_csv(outputdir, sia451.meta, sia451.chapters)
            else :
                print 'L\'exportation a échoué :'
                print "\n".join(sia451.errors)
                sys.exit(1)
