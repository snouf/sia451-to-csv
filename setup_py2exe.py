# setup.py
from distutils.core import setup
import py2exe


opts = {
	"py2exe": {
		#"includes": ["pango", "atk", "gobject", "gtk", "cairo", "gtk.glade"],
		#~ "dll_excludes": ["iconv.dll", "intl.dll",
			#~ "libatk-1.0-0.dll", "libgdk_pixbuf-2.0-0.dll",
			#~ "libgdk-win32-2.0-0.dll", "libglib-2.0-0.dll",
			#~ "libgmodule-2.0-0.dll", "libgobject-2.0-0.dll",
			#~ "libgthread-2.0-0.dll", "libgtk-win32-2.0-0.dll",
			#~ "libpango-1.0-0.dll", "libpangowin32-1.0-0.dll",
			#~ "libxml2", "libglade-2.0-0", "zlib1"
		#~ ]
	}
}

setup(
        name="sia451-to-csv",
        windows=["sia451-to-csv.py"],
        options=opts
	)
